#!/home/pi/Desktop/tradfri_home/env/bin/python
import sunfounder.LCD1602 as LCD1602
import sunfounder.temp_sensor as temp_sensor
import os
try:
    from config import DEFAULT_TEMPERATURE
except ImportError:
    from shutil import copy
    src = os.path.join(
                os.path.dirname(
                    os.path.abspath(__file__)), 'config_default.py')
    dest = os.path.join(
                os.path.dirname(
                    os.path.abspath(__file__)), 'config.py')
    copy(src, dest)
    from config import DEFAULT_TEMPERATURE


def setup():
    LCD1602.init(0x27, 1)


def run():
    while 1:
        from config import TEMP
        current_temp = temp_sensor.read()
        encoder_file = open("/home/pi/encoder.txt", "r")
        encoder = float(encoder_file.read())
        set_temp = DEFAULT_TEMPERATURE + encoder/10
        LCD1602.write(0, 0, "Set")
        LCD1602.write(8, 0, "Current")
        LCD1602.write(1, 1, "{:.1f}C".format(set_temp))
        LCD1602.write(9, 1, "{:.1f}C".format(current_temp))
        if set_temp != TEMP:
            config_file = open(
                os.path.join(
                    os.path.dirname(
                        os.path.abspath(__file__)), 'config.py'), 'r')
            config_contents = config_file.readlines()
            for i in config_contents:
                if "TEMP " in i:
                    config_contents[config_contents.index(i)] = "TEMP = {}\n".format(set_temp) # noqa
            config_file = open(
                os.path.join(
                    os.path.dirname(
                        os.path.abspath(__file__)), 'config.py'), 'w')
            config_file.write("".join(config_contents))
            config_file.close()


def destroy():
    LCD1602.clear()
    LCD1602.write(0, 0, "Manual Radiator")
    LCD1602.write(0, 1, "Control Required")


if __name__ == "__main__":
    setup()
    try:
        run()
    except KeyboardInterrupt:
        destroy()
    except Exception as e:
        LCD1602.clear()
        LCD1602.write(0, 0, "Error:{}".format(e))
