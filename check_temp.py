#!/home/pi/Desktop/tradfri_home/env/bin/python
import sunfounder.temp_sensor as temp_sensor
import numpy as np
import json
import os
from config import RAD_NAME, LIGHT_NAME, TEMP, LOG_FORMAT, DATE_FORMAT, NUM_TEMPS, FORECAST_TIME, FORECAST_TDIF, NUM_DATA # noqa
import sys
from datetime import datetime as dt
import logging as log
log.basicConfig(
    stream=sys.stderr,
    level=log.INFO,
    format=LOG_FORMAT,
    datefmt=DATE_FORMAT)

from pytradfri import Gateway
from pytradfri.api.libcoap_api import APIFactory
from pytradfri.util import load_json

PYTRADFRI_CONFIG_FILE = os.path.join(os.path.dirname(os.path.abspath(__file__)), "tradfri_standalone_psk.conf")

host = "192.168.86.49"
conf = load_json(PYTRADFRI_CONFIG_FILE)

identity = conf[host].get("identity")
psk = conf[host].get("key")
api_factory = APIFactory(host=host, psk_id=identity, psk=psk)
api = api_factory.request
gateway = Gateway()

devices_command = gateway.get_devices()
devices_commands = api(devices_command)
devices = api(devices_commands)



def get_temp_data():
    path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), 'temps.json')
    with open(path, "r") as infile:
        data = json.load(infile)
    return data


def get_pid_output():
    path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), 'pid_output.json')
    with open(path, "r") as infile:
        data = json.load(infile)
    return data


def get_all_data():
    try:
        path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)), 'all_data.json')
        with open(path, "r") as infile:
            data = json.load(infile)
        return data
    except (FileNotFoundError, json.decoder.JSONDecodeError):
        return [{
            "Socket": 0,
            "Temp": 18.0,
            "Light": 1}]


def keep_old_temp_data(temp):
    log.debug("Keeping old temp {}".format(temp))
    path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), 'all_temps.json')
    try:
        with open(path, "r") as infile:
            data = json.load(infile)
    except (FileNotFoundError, json.decoder.JSONDecodeError):
        data = []
    data.append(temp)
    data = data[-NUM_DATA:]
    with open(path, "w") as outfile:
        json.dump(data, outfile)


def add_temp_data(temp):
    try:
        data = get_temp_data()
    except (FileNotFoundError, json.decoder.JSONDecodeError):
        data = []
    # Keep the last NUM_TEMPS values
    while len(data) >= NUM_TEMPS:
        keep_old_temp_data(data.pop(0))
    data.append(temp)

    path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), 'temps.json')
    with open(path, "w") as outfile:
        json.dump(data, outfile)


def old_future_temp(minutes):
    temps = get_temp_data()
    x_vals = range(len(temps))
    poly = np.polyfit(x_vals, temps, 1)
    predicted = np.polyval(poly, max(x_vals) + minutes)
    log.debug("Predicted temp in {}mins: {}".format(minutes, predicted))
    return predicted


def time_to_temp(temp):
    #return
    temps = get_temp_data()
    x_vals = range(len(temps))
    poly = np.polynomial.Polynomial.fit(x_vals, temps, 1)
    roots = (poly - temp).roots()
    minutes = roots[0]
    log.debug("Predicted temp in {}mins: {}".format(minutes, temp))


def future_temp(minutes):
    data = get_all_data()
    data = data[-NUM_TEMPS:]
    final_data = []
    socket = data[-1].get("Socket")
    changes = 0
    for i in data:
        if i.get("Socket") == socket:
            final_data.append(i)
            changes = 0
        else:
            final_data.append(i)
            changes += 1
            if changes > 2:
                final_data = []
    temps = []
    for i in final_data:
        temps.append(i.get("Temp"))
    x_vals = range(len(temps))
    poly = np.polyfit(x_vals, temps, 1)
    predicted = np.polyval(poly, max(x_vals) + minutes)
    if np.isnan(predicted):
        log.debug("Prediction Failed, defaulting to old prediction")
        predicted = old_future_temp(minutes)
    log.debug("Predicted temp in {}mins: {}".format(minutes, predicted))
    return predicted


def pid_rad_control(socket=None):
    if socket is None:
        socket = next(filter(lambda x: x.name == RAD_NAME, devices))

    temperature = temp_sensor.read()
    log.debug("Temperature: {}".format(temperature))
    add_temp_data(temperature)

    light = next(filter(lambda x: x.name == LIGHT_NAME, devices))
    if light.light_control.lights[0].state:
        cycle_length = 5
        output = get_pid_output()
        rounded_output = round(output * cycle_length)
        cycle_point = dt.now().minute % cycle_length
        if rounded_output > cycle_point:
            log.debug("Pid On - Output: {}, Cycle Point: {}".format(output, cycle_point))
            socket.socket_control.set_state(True)
        else:
            log.debug("Pid Off - Output: {}, Cycle Point: {}".format(output, cycle_point))
            socket.socket_control.set_state(False)
    else:
        log.debug("No Light, so turning off")
        socket.socket_control.set_state(False)

    return {
        "Socket": socket.socket_control.sockets[0].state,
        "Temp": temperature,
        "Light": light.light_control.lights[0].state}


def pwm_rad_control(socket=None, cutoff_temp=TEMP):
    #time_to_temp(cutoff_temp)
    if socket is None:
        socket = next(filter(lambda x: x.name == RAD_NAME, devices))

    temperature = temp_sensor.read()
    log.debug("Temperature: {}".format(temperature))
    add_temp_data(temperature)

    light = next(filter(lambda x: x.name == LIGHT_NAME, devices))
    if light.light_control.lights[0].state:
        # If heating
        pred_temp = future_temp(FORECAST_TIME)
        if socket.socket_control.sockets[0].state:
            # If will be higher within forecast time
            if pred_temp > cutoff_temp - FORECAST_TDIF:
                if pred_temp > cutoff_temp:
                    log.debug("Pre-emptively Turning Off")
                    socket.socket_control.set_state(False)
                elif dt.now().minute % 2:
                    log.debug("PWM Off")
                    socket.socket_control.set_state(False)
                else:
                    log.debug("PWM On")
                    socket.socket_control.set_state(True)
        else:
            # If will be lower within forecast time
            if pred_temp < cutoff_temp:
                if pred_temp < cutoff_temp - FORECAST_TDIF:
                    log.debug("Pre-emptively Turning On")
                    socket.socket_control.set_state(True)
                elif dt.now().minute % 2:
                    log.debug("PWM Off")
                    socket.socket_control.set_state(False)
                else:
                    log.debug("PWM On")
                    socket.socket_control.set_state(True)
            elif temperature < cutoff_temp:
                if dt.now().minute % 5:
                    log.debug("PWM 20% On")
                    socket.socket_control.set_state(True)
                else:
                    log.debug("PWN 20% Off")
                    socket.socket_control.set_state(False)
    else:
        log.debug("No Light, so turning off")
        socket.socket_control.set_state(False)

    return {
        "Socket": socket.socket_control.sockets[0].state,
        "Temp": temperature,
        "Light": light.light_control.lights[0].state}


def rad_control(socket=None, cutoff_temp=TEMP):
    if socket is None:
        socket = next(filter(lambda x: x.name == RAD_NAME, devices))

    temperature = temp_sensor.read()
    log.debug("Temperature: {}".format(temperature))
    add_temp_data(temperature)

    light = next(filter(lambda x: x.name == LIGHT_NAME, devices))
    if light.light_control.lights[0].state:
        # If heating
        if socket.socket_control.sockets[0].state:
            # If will be higher within forecast time
            if future_temp(FORECAST_TIME) > cutoff_temp:
                log.debug("Pre-emptively Turning Off")
                socket.socket_control.set_state(False)
        else:
            # If will be lower within forecast time
            if future_temp(FORECAST_TIME) < cutoff_temp:
                log.debug("Pre-emptively Turning On")
                socket.socket_control.set_state(True)
    else:
        log.debug("No Light, so turning off")
        socket.socket_control.set_state(False)

    return {
        "Socket": socket.socket_control.sockets[0].state,
        "Temp": temperature,
        "Light": light.light_control.lights[0].state}


def save_data(newdata, filename='all_data.json'):
    try:
        path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)), filename)
        with open(path, "r") as infile:
            data = json.load(infile)
    except (FileNotFoundError, json.decoder.JSONDecodeError):
        data = []
    data.append(newdata)
    data = data[-NUM_DATA:]

    path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), filename)
    with open(path, "w") as outfile:
        json.dump(data, outfile)


if __name__ == '__main__':
    if len(sys.argv) == 2:
        if sys.argv[1] == "PID":
            result = pid_rad_control()
            save_data(result)
            log.info(result)
        elif sys.argv[1] == "PWM":
            result = pwm_rad_control()
            save_data(result)
            log.info(result)
        else:
            rad_control(cutoff_temp=float(sys.argv[1]))
    else:
        result = rad_control()
        save_data(result)
        log.info(result)
