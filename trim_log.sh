#!/bin/bash

if ! [ $1 ]
then
    echo "Usage: trim_log.sh <num>"
    exit 1
fi

tail -$1 log.log > tmp.log
cp tmp.log log.log
rm tmp.log