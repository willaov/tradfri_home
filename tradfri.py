# Docs for tradfri - https://github.com/glenndehaan/ikea-tradfri-coap-docs
import os
import configparser
import json
from func_timeout import func_set_timeout
from retrying import retry

# Tradfri Configuration
tradfriconf = configparser.ConfigParser()
tradfriconf.read(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'tradfri.cfg')) # noqa

tradfrihubip = tradfriconf.get('tradfri', 'hubip')
tradfrisecurityid = tradfriconf.get('tradfri', 'securityid')
tradfriclientid = tradfriconf.get('tradfri', 'clientid')

coap = '/usr/local/bin/coap-client'

default_cmd = coap + ' -m put -u "{clientid}" -k "{securityid}" -e \'{{ "3311": [{tradfricmd}] }}\' "coaps://{hubip}:5684/15001/{lightid}"' # noqa
socket_cmd = coap + ' -m put -u "{clientid}" -k "{securityid}" -e \'{{ "3312": [{tradfricmd}] }}\' "coaps://{hubip}:5684/15001/{lightid}"' # noqa
get_cmd = coap + ' -m get -u "{clientid}" -k "{securityid}" "coaps://{hubip}:5684/15001/{lightid}"' # noqa
get_output = " | awk 'NR==4'"

switch_cmd = "5850"     # On/Off
dim_cmd = "5851"        # Dimmer (1-254)
colourX = "5709"         # Colour X
colourY = "5710"         # Colour Y
colour_temp = "5711"    # Colour Temperature (250-454)

LIGHTS = ("Colour", "Temperature")
SOCKETS = ("Socket")


class Light():
    def __init__(self, lightid):
        self._lightid = lightid
        info = get_info(self.lightid)
        self._name = info["9001"]
        if "CWS" in info["3"]["1"]:
            self._type = "Colour"
        elif "WS" in info["3"]["1"]:
            self._type = "Temperature"
        else:
            self._type = None
        self._status = None
        self.update_status()

    @property
    def lightid(self):
        return self._lightid

    @property
    def status(self):
        self.update_status()
        return self._status

    @property
    def name(self):
        return self._name

    @property
    def type(self):
        return self._type

    def update_status(self):
        self._status = get_status(self)

    def on(self):
        command = Command(device=self, switch=1)
        command.send()

    def off(self):
        command = Command(device=self, switch=0)
        command.send()

    def toggle(self):
        new = int(not self.status['switch'])
        command = Command(device=self, switch=new)
        command.send()

    def dim_to(self, level):
        command = Command(device=self, dim=level)
        command.send()

    def dim(self, step=-10):
        newlevel = self.status['dim'] + step
        command = Command(device=self, dim=newlevel)
        command.send()


class Socket():
    def __init__(self, lightid):
        self._lightid = lightid
        info = get_info(self.lightid)
        self._name = info["9001"]
        self._type = "Socket"
        self._status = None
        self.update_status()

    @property
    def lightid(self):
        return self._lightid

    @property
    def status(self):
        self.update_status()
        return self._status

    @property
    def name(self):
        return self._name

    @property
    def type(self):
        return self._type

    def update_status(self):
        self._status = get_status(self)

    def on(self):
        command = Command(device=self, switch=1)
        command.send()

    def off(self):
        command = Command(device=self, switch=0)
        command.send()

    def toggle(self):
        new = int(not self.status['switch'])
        command = Command(device=self, switch=new)
        command.send()


class Command():
    def __init__(self, device, switch=None, dim=None, colour=None):
        self.device = device
        tradfricmd = {}
        if switch is not None:
            tradfricmd[switch_cmd] = switch
        if dim is not None:
            tradfricmd[dim_cmd] = dim
        if colour is not None:
            if colour.get('X') is not None:
                tradfricmd[colourX] = colour['X']
                tradfricmd[colourY] = colour['Y']
            elif colour.get('temp') is not None:
                tradfricmd[colour_temp] = colour['temp']
        tradfricmd = json.dumps(tradfricmd)
        if device.type in LIGHTS:
            self.cmd = build_cmd(
                tradfricmd, self.device.lightid) + get_output
        elif device.type in SOCKETS:
            self.cmd = build_socket_cmd(
                tradfricmd, self.device.lightid) + get_output
        else:
            raise TypeError

    @retry(stop_max_attempt_number=5)
    @func_set_timeout(5)
    def send(self):
        if os.path.exists(coap):
            result = os.popen(self.cmd)
        else:
            raise NotImplementedError
        return result.read()


def build_cmd(tradfricmd, lightid):
    cmd = default_cmd.format(
        clientid=tradfriclientid,
        securityid=tradfrisecurityid,
        tradfricmd=tradfricmd,
        hubip=tradfrihubip,
        lightid=lightid)
    return cmd


def build_socket_cmd(tradfricmd, lightid):
    cmd = socket_cmd.format(
        clientid=tradfriclientid,
        securityid=tradfrisecurityid,
        tradfricmd=tradfricmd,
        hubip=tradfrihubip,
        lightid=lightid)
    return cmd


def build_get_cmd(lightid=None):
    if lightid is None:
        lightid = ''
    cmd = get_cmd.format(
        clientid=tradfriclientid,
        securityid=tradfrisecurityid,
        hubip=tradfrihubip,
        lightid=lightid)
    return cmd


def get_status(device):
    cmd = build_get_cmd(lightid=device.lightid)
    cmd += get_output
    result = os.popen(cmd)
    result = result.read()
    result_json = json.loads(result.strip('\n'))
    if device.type in LIGHTS:
        result_status = result_json["3311"][0]
    elif device.type in SOCKETS:
        result_status = result_json["3312"][0]

    if device.type in LIGHTS:
        status = {}
        status["switch"] = result_status[switch_cmd]
        status["dim"] = result_status[dim_cmd]
        return status
    elif device.type in SOCKETS:
        status = {}
        status["switch"] = result_status[switch_cmd]
        return status
    else:
        raise NotImplementedError


def get_info(lightid=None):
    if lightid is None:
        cmd = build_get_cmd(lightid="")
        cmd += get_output
        result = os.popen(cmd)
        result = result.read()
        result_json = json.loads(result.strip('\n'))
        return result_json
    else:
        cmd = build_get_cmd(lightid=lightid)
        cmd += get_output
        result = os.popen(cmd)
        result = result.read()
        result_json = json.loads(result.strip('\n'))
        return result_json


if __name__ == "__main__":
    lamp = Light(65537)
    socket = Socket(65542)
    try:
        while True:
            if input(lamp.status):
                lamp.dim()
            else:
                lamp.toggle()
                socket.toggle()
    except Exception as e:
        print(e)
    finally:
        lamp.dim_to(254)
        lamp.on()
        print("Done")
