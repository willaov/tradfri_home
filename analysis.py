#!/home/pi/Desktop/tradfri_home/env/bin/python
import os
import sys
import json
import datetime
from config import DATE_FORMAT, GRAPHING
import numpy as np
import sunfounder.temp_sensor as temp_sensor


def plot_all_data(prange=None, graphing=GRAPHING):
    cmd = "grep '^[[]INFO.*{.*Socket.*}$' /home/pi/Desktop/tradfri_home/log.log" # noqa
    result = os.popen(cmd)
    read = result.read().strip('\n')
    lines = read.split('\n')
    temps = []
    times = []
    on_temps = []
    on_times = []
    off_temps = []
    off_times = []
    datas = []
    # Range is a tuple
    if prange is not None:
        lines = lines[prange[0]:prange[1]]
    for line in lines:
        time = " ".join(line.split()[1:3]).strip(':')
        time = datetime.datetime.strptime(time, DATE_FORMAT)
        data = ('{' + line.split('{')[-1]).replace("'", '"')
        data = data.replace("True", "true")
        data = data.replace("False", "false")
        data_dict = json.loads(data)
        datas.append((time, data_dict))
        temps.append(data_dict['Temp'])
        times.append(time)
        if data_dict['Socket'] == 1:
            on_temps.append(data_dict['Temp'])
            on_times.append(time)
        elif data_dict['Socket'] == 0:
            off_temps.append(data_dict['Temp'])
            off_times.append(time)
    if graphing == "Matplotlib":
        from matplotlib import pyplot as plt
        plt.plot(
            on_times, on_temps,
            marker='o', markersize=2, linestyle='None', color='r')
        plt.plot(
            off_times, off_temps,
            marker='o', markersize=2, linestyle='None', color='b')
        plt.show()
    elif graphing == "Plotly":
        import plotly.graph_objects as go
        fig = go.Figure()
        fig.add_trace(
            go.Scatter(
                x=on_times,
                y=on_temps,
                name="Heater On",
                mode='markers',
                marker=dict(
                    color='red'
                )))
        fig.add_trace(
            go.Scatter(
                x=off_times,
                y=off_temps,
                name="Heater Off",
                mode='markers',
                marker=dict(
                    color='blue'
                )))
        fig.update_xaxes(rangeslider_visible=True)
        # fig.show()
        fig.write_html("/home/pi/Desktop/tradfri_home/www/out.html")
    return {
        "on_times": on_times,
        "off_times": off_times,
        "on_temps": on_temps,
        "off_temps": off_temps,
        "temps": temps,
        "times": times,
        "datas": datas}


if __name__ == "__main__":
    if len(sys.argv) == 2:
        if sys.argv[1] == "plot":
            plot_all_data(prange=[-10080, -1])
