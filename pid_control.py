#!/home/pi/Desktop/tradfri_home/env/bin/python
from simple_pid import PID
import sunfounder.temp_sensor as temp_sensor
import os
import json
import time

pid = PID(1.0, 0.1, 0.1)
pid.sample_time = 10
pid.output_limits = (0, 1)


def set_output(value):
    path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), 'pid_output.json')
    with open(path, "w") as outfile:
        json.dump(value, outfile)


def run():
    while True:
        from config import TEMP
        pid.setpoint = TEMP
        temperature = temp_sensor.read()
        output = pid(temperature)
        set_output(output)
        print(output, time.time())
        time.sleep(2)


if __name__ == "__main__":
    run()
